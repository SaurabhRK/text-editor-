#include "move.h"

void move_up(win w, int *y, int *x, int *y_offset) {
	if(*y > 0)
		*y = *y - 1;
	else if(*y_offset>0)
		*y_offset = *y_offset -  1;	
	if(*x > strlen(w.winln[*y + *y_offset].lns)) {
		*x = strlen(w.winln[*y + *y_offset].lns) - 1;
	}
	
}

void move_down(win w, int *y, int *x, int *y_offset, int ROWS) {
	if(*y < ROWS - 1 && *y < w.line_no  )	
		*y = *y + 1;
	else if(*y + *y_offset == w.line_no + 1)
		*y = *y;
	else if( *y + *y_offset < w.line_no - 1)
		*y_offset = *y_offset + 1;
	if(*x > strlen(w.winln[*y + *y_offset].lns)) {
		if(strlen(w.winln[*y + *y_offset].lns) == 0)
			*x = 0;
		else
			*x = strlen(w.winln[*y + *y_offset].lns) - 1;
	}
	//if(*y >= ROWS - 1)	
	//if(*y_offset > 0)
		//*y_offset = *y_offset + 1;
}

void move_left(win w, int *y, int *x) {
	if(*x > 0)	
		*x = *x - 1;
}

void move_right(win w, int *y, int *x, int *y_offset) {
	if(strlen(w.winln[*y + *y_offset].lns)-1 > *x)	{
		*x = *x + 1;
		if(*y == w.line_no)
			*x = *x + 1;
	}					
}
 //write move in main	
