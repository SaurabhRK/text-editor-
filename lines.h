#define SIZE 148
#include<stdlib.h>
#include<ncurses.h>
#include<string.h>
typedef struct line {
	char *lns;
	int size;
}line;

void init_line(line *l, int COLUMNS);
void add_char(line *l, int c, int x);
void rem_char(line *l, int x);
//void print_line(line *l, int y, int x);
