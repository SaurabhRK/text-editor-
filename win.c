#include "win.h"
#include<stdlib.h>
#include<string.h>

void init_win(win *w, int size) {              
	w->winln = (line*)malloc(sizeof(line)*size);
	w->line_no = 0;
	w->num_lines = size;
}

void insert_line(win *w, int y, int COLS, int x) {
	line new;
	int j, k = 0;
	char c;
	init_line(&new,COLS);
	if(w->line_no >= w->num_lines - 1)
		expand(w);
	if(w->line_no >= y && w->line_no != 0) 
		in_between(w, y, COLS, x);
	else {
		if(x != 0) {
			for(j = x; w->winln[y-1].lns[j] != '\0'; j++) { 	//if user presses enter while there is no newline inserted
				c = w->winln[y-1].lns[j];             	
				add_char(&new, c, k);				//shifthing data to next line where it Enter is pressed	
				k++;
			}
			w->winln[y-1].lns[x] = '\n';
			w->winln[y-1].lns[x+1] = '\0';                           //data deletion from above line
		}
	
		w->winln[y] = new;
		w->line_no++;

	}
}

/*void print_win(win *w) {
	int i;
	for(i = 0; i <= w->line_no; i++) {
		clrtoeol();
		move(i, 0);
		printw("%s", w->winln[i].lns);
	}
}*/

void in_between(win *w, int y, int COLS, int x) {
	line new;
	char c;
	int i, j, k = 0;
	init_line (&new, COLS);
	w->winln[w->line_no + 1] = new;
	for(i = w->line_no; i>= y; i--)
		w->winln[i+1] = w->winln[i];
	init_line(&w->winln[y], COLS);
	for(j = x; w->winln[y-1].lns[j] != '\0'; j++) {
		c = w->winln[y-1].lns[j];		
		add_char(&w->winln[y], c, k);
		k++;
	}
	w->winln[y-1].lns[x] = '\n';
	w->winln[y-1].lns[x+1] = '\0';
	w->line_no++;
}

void expand(win *w) {
	int size, i = 0;
	size = 2*w->num_lines;
	line *l;
	l = (line*)malloc(sizeof(line)*size);
	for(i = 0; i < w->num_lines; i++)
		l[i] = w->winln[i];
	w->winln = l;
	w->num_lines = size;
}
	
void rem_line(win *w, int y) {
	int i = 0;
	free(w->winln[y].lns);
	for(i = y ; i <= w->line_no; i++)
		w->winln[i] = w->winln[i+1];
	w->line_no--;
} 



