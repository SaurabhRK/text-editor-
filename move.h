#include<string.h>
#include"win.h"

void move_up(win w, int *y, int *x, int *y_offset);
void move_down(win w, int *y, int *x, int *y_offset, int ROWS);
void move_left(win w, int *y, int *x);
void move_enter(win w, int *y, int *x);
void move_right(win w, int *y, int *x, int *y_offset);


