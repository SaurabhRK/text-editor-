#include"lines.h"

void init_line(line *l, int ROWS) {
	l->size = ROWS;
	l->lns = (char*)malloc((l->size)*sizeof(char));
	l->lns[0] = '\0';
}

void add_char(line *l, int c, int x) {
	int i, z;
	z = strlen(l->lns);
	for(i = z; i >= x; i--)
		l->lns[i+1] = l->lns[i];
	l->lns[x] = c;
}

void rem_char(line *l, int x) {
	int i, z;
	z = strlen(l->lns);
	for(i = x; i < z; i++)
		l->lns[i] = l->lns[i+1];
}

										
