								    		##   TEXT EDITOR  ##	 




Project Title: Text Editor
Name: Saurabh Rajesh Kshirsagar
MIS : 111708030
 
Work done on Project:
	1) Designing of data structure for text editor
	2) Basic input output functions of Ncurses
	3) Screen manupulation using Ncurses
	4) Prompt Designing
	5) Implemented following Functions in text editor
		a) Read, write and save in existing file
		b) Create, write and save in new file given by user
		c) Insertion of charcters like new_line or tab and showing them on screen
		d) Cursur movement like VI
		e) Some basic functions along with their animation on screen like
			i)   To Save
			ii)  To Quit
			iii) To Search
			iV)  To Replace
			V)   To Copy a word as well as a Line
			Vi)  To Cut a word as well as a Line
			Vi)  To Paste
			vii) To Delete a Line
	
