#include"move.h"
#include<stdio.h>
#include<curses.h>
#define TAB 8
#include"file.h"
int exists(char *a);
void print_prompt();
void read_file(win *w, char *filename);
void print_loc(int y, int x, int y_offset);
void replace(win *w, int ROWS);
void search(win *w, int ROWS);
void copy(win *w, int y, char **string);
void copy_word(win *w, int y, int x, char **string);
void paste(win *w, int y, int x, char *string);
void cut(win *w, int y, int x, char cut_func, char **string);
void print_line(win w, int y);
int highlight(char *info, char *file_name, int ROWS);
void quit(win *w, int ROWS);
int y_offset = 0;

int main(int argc, char *argv[]) {
	int x, y, k, r, v = 0, m = 0, j = 0;
	int ch, COLS, ROWS;
	char *c, *string, copy_func, cut_func;
	win w;
	cbreak();
	keypad(stdscr, TRUE);
	getmaxyx(stdscr, ROWS, COLS);
	if(argc > 1) {
		if(exists(argv[1]))
			read_file(&w, argv[1]);
		else {
			initscr();
			raw();
			noecho();
			getmaxyx(stdscr, ROWS, COLS);
			init_win(&w, ROWS);
			insert_line(&w, 0, ROWS, 0);
			w.line_no = 0;
		}
	}
	move(0, 0);
	getyx(stdscr, y, x);
	getmaxyx(stdscr, ROWS, COLS);
	while(1) {
		print_win(&w, y_offset, ROWS + y_offset);
		print_loc(y, x, y_offset);
		keypad(stdscr, TRUE);
		getyx(stdscr, y, x);
		ch = getch();
		switch((unsigned int)(ch)) {
			case KEY_F(5):
				c =  "Saving file as : ";
				j = highlight(c ,argv[1], ROWS);
				if( j == 1)
					save_file(&w, argv[1]);	
				clear();
				break;
			case KEY_F(7):
				c = "Quiting the file";
				m = highlight(c, "", ROWS);
				if( m == 1)
					quit(&w, ROWS);
				clear();
				break;
			case KEY_F(2):
				search(&w, ROWS);
				clear();
				break;
			case KEY_F(3):
				replace(&w, ROWS);
				clear();
				break;
			case KEY_F(4):
				copy_func = getch();
				if(copy_func == 'L')
					copy(&w, y + y_offset, &string);
				else if(copy_func == 'W')
					copy_word(&w, y + y_offset, x, &string);
				else if(copy_func == 27);
					break;
				break;
			case KEY_F(6):
				paste(&w, y + y_offset, x, string);
				print_win(&w,y_offset, ROWS + y_offset - 1);
				break;
			case KEY_DC:
				if(x == 0) {
					rem_line(&w, y + y_offset);
				}
				break; 
			case KEY_F(8):
				cut_func = getch();
				if(cut_func == 'L' || cut_func == 'W')
					cut(&w, y + y_offset, x, cut_func, &string);
				else if(cut_func == 27)
					break;
				break;
			case KEY_BACKSPACE:
				if(x > 0) {
					move(y, --x);				
					rem_char(&w.winln[y+y_offset],x);
					clrtoeol();
					print_win(&w,y_offset, ROWS + y_offset - 1);
					refresh();
					move(y, x);
					print_loc(y, x, y_offset);
				}
				break;
			case '\n':
				x = 0;
				if(y_offset > 0)
					y_offset++;
				else
					y++;
				getyx(stdscr, k, r);
				insert_line(&w,y + y_offset, COLS, r);
				move(y, 0);
				print_win(&w, y_offset, ROWS + y_offset - 1);
				refresh();
				break;
			case '\t':
				for(v = 0; v < TAB; v++) {
					addch(' ');
					add_char(&w.winln[y+y_offset], ' ', x);
					x++;
				}
				break;
			case KEY_LEFT:
				move_left(w, &y, &x);
				move(y, x);
				break;
			case KEY_RIGHT:
				move_right(w, &y, &x, &y_offset);				
				move(y, x);
				break;
			case KEY_UP:
				move_up(w, &y, &x, &y_offset);
				move(y, x);
				refresh();
				break;
			case KEY_DOWN:
				move_down(w, &y, &x, &y_offset, ROWS);
				move(y, x);
				refresh();
				break; 
			default:
				addch(ch);
				add_char(&w.winln[y+y_offset], ch, x);
				move(y, ++x);
				break;
		}
		print_win(&w, y_offset, ROWS + y_offset - 1);
		refresh();
		if(ch ==KEY_F(7) && m == 1) {
			break;
		}
	}
	endwin();
	return 0;
}
void print_loc(int y, int x, int y_offset) {
	mvprintw(0, COLS - 22, "x: %d y: %d y_off:%d ", x, y, y_offset);
	move(y, x);
}

void print_line(win w, int y) {
	int i = 0;
	clear();
	for(i = 0; i <= y; i++) {
		clrtoeol();		
		mvprintw(0, i, "%s", w.winln[i].lns);
		refresh();
	}
	refresh();
}
void print_win(win *w, int start_win, int end_win) {
	int i = 0, z = 0;
	for(i = start_win, z = 0; i < end_win && i <= w->line_no ; i++, z++) {
		clrtoeol();
		move(z, 0);
		printw("%s", w->winln[i].lns);
	}
	print_prompt();
	refresh();
}
void print_prompt() {
	int prev_y, prev_x;
	getyx(stdscr, prev_y, prev_x);
	attron(A_BOLD);
	mvprintw(1, COLS - 22, "F5 : To save ");
	mvprintw(2, COLS - 22, "F7 : To Quit ");
	mvprintw(3, COLS - 22, "F3 : To replace ");
	mvprintw(4, COLS - 22, "F2 : To search ");
	mvprintw(5, COLS - 22, "DEL : To Delete line ");
	mvprintw(6, COLS - 22, "F4 + L : To Copy line ");
	mvprintw(7, COLS - 22, "F4 + W : To Copy Word ");
	mvprintw(8, COLS - 22, "F6 : To Paste ");
	mvprintw(9, COLS - 22, "F8 + W : To Cut Word ");
	mvprintw(10, COLS - 22, "F8 + L : To Cut Line ");
	mvprintw(11, COLS - 22, "ESC : To Escape ");
	attroff(A_BOLD);
	move( prev_y, prev_x);
}
void read_file(win *w, char *filename) {
	FILE *fp;
	int ch;
	int ROWS, COLS;
	int x = 0, y = 0, v = 0;
	fp = fopen(filename, "r");
	initscr();
	raw();
	noecho();
	getmaxyx(stdscr, ROWS, COLS);
	init_win(w, ROWS);
	insert_line(w, 0, ROWS, 0);
	w->line_no = 0;
	ch = fgetc(fp);
	while(ch != EOF) {
		switch((unsigned int) ch) {
			case '\n':
				add_char(&w->winln[y], ch, x);
				x = 0;
				y++;
				insert_line(w,y, COLS, x);
				break;
			case '\t':
				for(v = 0; v < TAB; v++) {
					addch(' ');
					add_char(&w->winln[y+y_offset], ' ', x);
					x++;
				}
				break;
			default:
				addch(ch);
				add_char(&w->winln[y], ch, x);
				x++;
				break;
		}
		ch = fgetc(fp);
	}
	fclose(fp);
}	

int exists(char *a) {
	FILE *fp;
	fp = fopen(a, "r");
	if(fp != NULL) 
		return 1;
	else
		return 0;
}
				 
void save_file(win *w, char *file_name) {
	FILE *fp;
	fp = fopen(file_name, "w+");
	int y, x;
	for(y = 0; y <= w->line_no ; y++) {
		x = 0;
		while(w->winln[y].lns[x] != '\0') {
			fputc(w->winln[y].lns[x], fp);
			x++;
		}
	}
	fclose(fp);
}

int highlight(char *info, char *file_name, int ROWS) {
	int y_prev, x_prev, ch;
	getyx(stdscr, y_prev, x_prev);
	attron(A_REVERSE);
	move(ROWS - 2, 0);
	clrtoeol();
	printw( "%s %s \n", info, file_name);
	printw("Are you sure Y / N : ");
	attroff(A_REVERSE);
	printw("  ");
	ch  = getch();
	addch(ch);
	refresh();
	if(ch == 'Y')
		return 1;
	move(y_prev, x_prev);
	return 0;
}

void search(win *w, int ROWS) {
	char d[30];
	int y_prev, x_prev,i = 0, ch, x, y = 0, FLAG = 0, y_offset = 0, g, h;
	getyx(stdscr, y_prev, x_prev);
	move(ROWS - 1, 0);
	clrtoeol();
	noecho();
	attron(A_BOLD);
	printw( "Enter the Word You want to search : ");
	for(i = 0; ;i++) {
		ch = getch();
		if(ch == 127) {
			getyx(stdscr, g, h);
			move(g, h - 1);
			addch(d[i-1]);
			i--;
		}
		else if(ch == '\n')
			break;
		else {
			d[i] = ch;
			addch(ch);
		}
	}
	d[i] = '\0';
	attroff(A_BOLD);
	print_win(w, 0, ROWS);
	refresh();
	i = 0;
	y = 0;
	for(y = 0; y <= w->line_no; y++) {
		if(y > ROWS - 1) 
			y_offset = y_offset + 1;
		for(x = 0; x < strlen(w->winln[y].lns); x++) {
			if(w->winln[y].lns[x] == d[i]) {
				FLAG++;
				i++;
			}
			else {
				FLAG = 0;
				i = 0;
			}
			if(FLAG == strlen(d)) {
				if(y_offset > 0) {
					print_win(w, y_offset, ROWS + y_offset);
					move(ROWS - 1,  (x - strlen(d) + 1));
					attron(A_BOLD);
					printw("%s", d);
					attroff(A_BOLD);
					move(ROWS - 1,  (x - strlen(d) + 1));
				}
				else {
					move(y,  (x - strlen(d) + 1));
					attron(A_BOLD);
					printw("%s", d);
					move(y,  (x - strlen(d) + 1));
					attroff(A_BOLD);
				}
				ch = getch();
				if(ch == 27) {
					getyx(stdscr, g, h);
					break;
				}
				else if(ch == KEY_DOWN) {
					FLAG = 0;
					i = 0;
					continue;
				}
			}
		}
		if(ch == 27) {
			move(g, h);
			break;
		}
		else {
			FLAG = 0;
			i = 0;
		}
	}
}

void replace(win *w, int ROWS) {
	char d[30], e[30];
	int y_prev, x_prev, i = 0, ch, x, y, FLAG = 0,  j = 0, y_offset = 0, g, h, m = 0;
	getyx(stdscr, y_prev, x_prev);
	move(ROWS - 2, 0);
	attron(A_BOLD);
	clrtoeol();
	printw( "Enter the Word You want to replace: ");
	for(i = 0; ;i++) {
		ch = getch();
		if(ch == 127) {
			getyx(stdscr, g, h);
			move(g, h - 1);
			addch(d[i-1]);
			i--;
		}
		else if(ch == '\n')
			break;
		else {
			d[i] = ch;
			addch(ch);
		}
	}
	d[i] = '\0';
	printw( "\n");
	clrtoeol();
	printw( "Replacement  : ");
	for(i = 0; ;i++) {
		ch = getch();
		if(ch == 127) {
			getyx(stdscr, g, h);
			move(g, h - 1);
			addch(d[i-1]);
			i--;
		}
		else if(ch == '\n')
			break;
		else {
			e[i] = ch;
			addch(ch);
		}
	}
	e[i] = '\0';
	i = 0;
	for(y = 0; y <= w->line_no; y++) {
		if(y > ROWS - 1) 
			y_offset++;
		for(x = 0; x < strlen(w->winln[y].lns); x++) {
			if(w->winln[y].lns[x] == d[i]) {
				FLAG++;
				i++;
			}
			else {
				FLAG = 0;
				i = 0;
			}
			if(FLAG == strlen(d)) {
				m = x-strlen(d) + 1;
				for(h = 0; h < strlen(d); h++) {
					rem_char(&w->winln[y],  m);					
				}
				g = x-strlen(d) + 1;
				j = strlen(e) - 1;
				for(j = strlen(e) - 1; j > -1; j--)
					add_char(&w->winln[y], e[j], m);
				FLAG = 0;
				i = 0;
				continue;
			}
		}
	}
	move(y_prev, x_prev);	
} 
				

void write_file(win *w, char *file_name) {
	FILE *fp;
	fp = fopen(file_name, "w+");
	fclose(fp);
} 

void copy(win *w, int y, char **ch) {
	*ch = (char*) malloc(sizeof(char) * (strlen(w->winln[y].lns) + 1));
	strcpy(*ch, w->winln[y].lns);
}
	
void paste(win *w, int y, int x, char *ch) {
	int i = 0, j = 0;
	for(i = x; j < strlen(ch) ; i++, j++) {
		if(ch[j] == '\n')
			break;
		add_char(&w->winln[y], ch[j], i);
	}
}
	
void copy_word(win *w, int y, int x, char **ch) {
	int i = 0, z = 0;
	char d[30];
	*ch = (char*) malloc(sizeof(char) * (strlen(w->winln[y].lns) + 1));
	for(i = x; ; i++) {
		if(w->winln[y].lns[i] == 32 || w->winln[y].lns[i] == '\n')
			break;
		else {
			d[z] = w->winln[y].lns[i];
			z++;
		}
	}
	
	d[z] = '\n';
	d[z + 1] = '\0';
	strcpy(*ch, d);
}		
	
void cut(win *w, int y, int x, char cut_func, char **ch) {
	int i, z = 0;
	if(cut_func == 'W')
		copy_word(w, y, x, ch);
	else if(cut_func == 'L')
		copy(w, y, ch);
	for(i = 0; ;i++) {
		if(cut_func == 'W') {
			if(w->winln[y].lns[x] == 32 || w->winln[y].lns[x] == '\n' || w->winln[y].lns[x] == '\0')
			break;
			rem_char(&w->winln[y], x);
		}
		else if(cut_func == 'L') {
			if(w->winln[y].lns[z] == '\n' || w->winln[y].lns[z] == '\0')
			break;
			rem_char(&w->winln[y], z);
		}	
	}
}

void quit(win *w, int ROWS) {
	int i;
	for(i = 0; i <= w->line_no; i++) {
		free(w->winln[i].lns);
	}
	free(w->winln);
}














		

	
				








	
