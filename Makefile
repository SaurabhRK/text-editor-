project : lines.o win.o move.o orig_useline.o
	cc lines.o move.o orig_useline.o win.o -lncurses -o project
lines.o : lines.c lines.h
	cc -c lines.c -Wall 
win.o : win.c win.h
	cc -c win.c -Wall 
move.o : move.c move.h
	cc -c move.c -Wall 
orig_useline.o : orig_useline.c move.h file.h
	cc -c orig_useline.c -Wall 
