#include "lines.h"
typedef struct win{
	line *winln;
	int line_no;
	int num_lines;
}win;
        
void init_win(win *w, int size);
void insert_line(win *w, int y, int COLS, int x);
void print_win(win *w, int start_win, int end_win);
void in_between(win *w, int y, int COLS, int x);
void expand(win *w);
void rem_line(win *w, int y);

